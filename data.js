export const texts = [
  "Alice was beginning to get very tired of sitting by her sister on the bank, and of having nothing to do: once or twice she had peeped into the book her sister was reading, but it had no pictures or conversations in it",
  "Down, down, down. There was nothing else to do, so Alice soon began talking again. “Dinah ’ll miss me very much to-night, I should think !” (Dinah was the cat.) ",
  "There were doors all round the hall, but they were all locked, and when Alice had been all the way down one side and up the other, trying every door, she walked sadly down the middle, wondering how she was ever to get out again.",
  "After a while, finding that nothing more happened, she decided on going into the garden at once, but, alas for poor Alice!",
  "Soon her eye fell on a little glass box that was lying under the table: she opened it, and found in it a very small cake, on which the words “EAT ME” were beautifully marked in currants",
  "And she went on planning to herself how she would manage it. “They must go by the carrier,”she thought; “ and how funny it’ll seem, sending presents to one’s own feet!",
  "This time Alice waited patiently until it chose to speak again. In a minute or two the Caterpillar took the hookah out of its mouth, and yawned once or twice, and shook itself.",
];

export default { texts };
