export class Commentor {
  constructor() {
    this.element = document.getElementById("commentor");
    this.textElement = document.getElementById("commentor-message");
  }

  changeMessage(message) {
    this.textElement.innerHTML = message;
  }

  show() {
    this.element.classList.remove("display-none");
  }

  hide() {
    this.element.classList.add("display-none");
  }

  greetUsers(users) {
    let message = `Hello racers: `;

    users.forEach((user, index) => {
      if (users.length - 1 === index) {
        message += user.username;
      } else {
        message += user.username + ", ";
      }
    });
    this.changeMessage(message);
  }

  charsToFinishAlert(user) {
    const message = user.username + " +" + "FASTER 30 CHARS TO FINISH!";
    this.changeMessage(message);
  }

  announceWinner(user) {
    const message = user.username + " wins!";
    this.changeMessage(message);
  }
}
