import { Commentor } from "./commentor.mjs";

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });
const commentor = new Commentor();

const addRoomBtn = document.getElementById("add-room-btn");
const roomsElement = document.getElementById("rooms");
const roomsPageElement = document.getElementById("rooms-page");
const gamePageElement = document.getElementById("game-page");
const quitRoomBtn = document.getElementById("quit-room-btn");
const readyBtn = document.getElementById("ready-btn");
const timerElement = document.getElementById("timer");
const textContainerElement = document.getElementById("text-container");
const secondsForGameElement = document.getElementById("seconds-for-game");
const textInputElement = document.getElementById("text-input");
const modalElement = document.getElementById("modal");
const commentorElement = document.getElementById("commentor");
const commentorMessageElement = document.getElementById("commentor-message");

function createRoomButtonHandler() {
  const roomId = prompt("Enter the room name:", "");
  socket.emit("CREATE_ROOM", { roomId, roomCount: 0 });
}

function quitRoomBtnHandler() {
  roomsPageElement.classList.remove("display-none");
  gamePageElement.classList.add("display-none");
  readyBtn.innerText = "READY";
  socket.emit("LEAVE_ROOM");
}

function countDownBeforeGame(seconds) {
  let timeLeft = seconds;
  function intervalHandler() {
    if (timeLeft <= 0) {
      clearInterval(interval);
      socket.emit("START_GAME");
    }
    timer.innerText = timeLeft--;
  }
  intervalHandler();
  const interval = setInterval(intervalHandler, 1000);
}

function countDownInGame(seconds, user) {
  let timeLeft = seconds;
  function intervalHandler() {
    if (timeLeft <= 0) {
      socket.emit("USER_WIN", user);
      clearInterval(interval);
    }
    secondsForGameElement.innerText = timeLeft--;
  }
  intervalHandler();
  const interval = setInterval(intervalHandler, 1000);
}

function readyBtnHandler() {
  if (readyBtn.innerText === "NOT READY") {
    readyBtn.innerText = "READY";
  } else {
    readyBtn.innerText = "NOT READY";
  }
  socket.emit("USER_STATUS", username);
}

function getTextById(id) {
  return fetch(`http://localhost:3002/game/texts/${id}`)
    .then((body) => body.text())
    .then((data) => data);
}

// HELPERS
function updatePrgressBar([username, percent]) {
  const progressBar = document.querySelector(`.${username}`);
  progressBar.style.width = percent + "%";
  if (percent === 100) {
    progressBar.classList.add("finished");
  }
}

function resetProgressBars() {
  const progressBars = document.querySelectorAll(".user-progress");
  progressBars.forEach((progressBar) => {
    progressBar.style.width = 0;
    progressBar.classList.remove("finished");
  });
}

function getProgressPercent() {
  const textLength = textContainerElement.children.length;
  let exactCount = 0;
  const spans = textContainerElement.querySelectorAll("span");
  spans.forEach((span) => {
    if (span.classList.contains("exact")) {
      exactCount++;
    }
  });

  const percent = (exactCount / textLength) * 100;

  return percent;
}

function getNumberWithSign(number, sign) {
  return number + sign;
}

//Currying
const getNumberWithSignCurried = _.curry(getNumberWithSign);

function showModal(winners) {
  modalElement.classList.remove("display-none");
  textContainerElement.classList.add("display-none");
  const heading = document.createElement("h2");
  heading.textContent = "Results:";
  const closeButton = document.createElement("button");
  closeButton.textContent = "X";
  modalElement.append(heading);
  modalElement.append(closeButton);
  closeButton.addEventListener("click", () => {
    modalElement.classList.add("display-none");
    quitRoomBtn.classList.remove("display-none");
    readyBtn.classList.remove("display-none");
    resetProgressBars();
    winners.forEach((winner) => {
      winner.status = false;
      updateStatusHandler(winner);
    });
    modalElement.innerHTML = "";
  });
  winners.forEach((winner, index) => {
    const div = document.createElement("div");
    div.textContent = `${index + 1}. ${winner.username}`;
    modalElement.append(div);
  });
}

function game(text) {
  textInputElement.value = "";
  textContainerElement.innerHTML = "";
  text.split("").forEach((char) => {
    const span = document.createElement("span");
    span.innerText = char;
    textContainerElement.appendChild(span);
  });
}

socket.on("CLEAR_STORAGE", () => {
  window.location.replace("/login");
  sessionStorage.removeItem("username");
});

socket.on("SHOW_ERROR", (message) => {
  alert(message);
});

function renderRoom({ roomId, roomCount }) {
  const room = document.createElement("div");
  room.setAttribute("id", roomId);
  room.classList.add("room");
  const roomSpan = document.createElement("span");
  roomSpan.textContent = roomCount + " " + "users connected";
  const roomHeading = document.createElement("h2");
  roomHeading.textContent = roomId;
  const roomButton = document.createElement("button");
  roomButton.classList.add("join-btn");
  roomButton.textContent = "Join Room";
  roomButton.addEventListener("click", () => {
    socket.emit("JOIN_ROOM", username, roomId);

    roomsPageElement.classList.add("display-none");
    document.querySelector(".game-info").children[0].innerText = roomId;
    gamePageElement.classList.remove("display-none");
    quitRoomBtn.addEventListener("click", () => quitRoomBtnHandler(roomId));
  });
  room.append(roomSpan);
  room.append(roomHeading);
  room.append(roomButton);
  roomsElement.append(room);
}

socket.on("RENDER_ROOM", renderRoom);

function updateRoomsHandler(rooms) {
  roomsElement.innerHTML = "";
  rooms.forEach((room) => {
    renderRoom(room[1]);
  });
}

socket.on("UPDATE_ROOMS", updateRoomsHandler);

function updateRoomClientsNumber(roomName, count) {
  const room = document.getElementById(roomName);
  if (room) {
    const span = room.children[0];
    span.textContent = count + " " + "users connected";
  }
}

socket.on("update", updateRoomClientsNumber);

function updateNamesHandler(users) {
  const usersElement = document.querySelector(".users");

  usersElement.innerHTML = "";
  users.forEach((user) => {
    const userElement = document.createElement("div");
    userElement.classList.add("user", `user-${user.id}`);
    const span = document.createElement("span");
    span.textContent = user.username;
    const status = document.createElement("div");
    if (user.status) {
      status.classList.add("status", "ready-status-green");
    } else {
      status.classList.add("status", "ready-status-red");
    }
    const progressBar = document.createElement("div");
    progressBar.classList.add("progress");
    const progress = document.createElement("div");
    progress.classList.add("user-progress", user.username);
    progressBar.append(progress);

    userElement.append(status);
    userElement.append(span);
    userElement.append(progressBar);
    usersElement.append(userElement);
  });

  socket.emit("YOU");
}

socket.on("UPDATE_NAMES", updateNamesHandler);

function updateYouHandler(user) {
  if (user) {
    const userElement = document.querySelector(`.user-${user.id}`);
    const span = userElement.querySelector("span");
    span.innerHTML = user.username + "&nbsp;&nbsp;&nbsp;👈";
  }
}

socket.on("UPDATE_YOU", updateYouHandler);

function startTimerHandler(secondsBeforeStart) {
  quitRoomBtn.classList.add("display-none");
  readyBtn.classList.add("display-none");
  timerElement.classList.remove("display-none");
  countDownBeforeGame(secondsBeforeStart);
}

socket.on("START_TIMER", startTimerHandler);

async function showTextHandler(textId, user, secondsForGame) {
  commentor.show();
  socket.emit("GREET_USERS");
  commentorMessageElement.innerText = "Commentor";
  timerElement.classList.add("display-none");
  secondsForGameElement.classList.remove("display-none");
  countDownInGame(secondsForGame, user);
  textContainerElement.classList.remove("display-none");
  textInputElement.classList.remove("display-none");
  const text = await getTextById(textId);
  game(text);

  setTimeToEvents();
  textInputElement.addEventListener("input", textInputHandler);

  function textInputHandler() {
    const textArray = textContainerElement.querySelectorAll("span");
    const valueArray = textInputElement.value.split("");
    let exact = true;
    textArray.forEach((span, index) => {
      const char = valueArray[index];

      if (char == null) {
        span.classList.remove("exact");
        span.classList.remove("inexact");
        exact = false;
      } else if (char === span.innerText) {
        span.classList.add("exact");
        span.classList.remove("inexact");
      } else {
        span.classList.add("inexact");
        span.classList.remove("exact");
        exact = false;
      }
    });

    if (exact) {
      socket.emit("USER_WIN", user);
      socket.emit("ANNOUNCE_WINNER_COMMENTOR", user);
      secondsForGameElement.classList.add("display-none");
      textInputElement.classList.add("display-none");
      textInputElement.removeEventListener("input", textInputHandler);
    }

    const percent = getProgressPercent();

    socket.emit("REWRITE_LETTERS", { username, percent });

    socket.emit("INPUT_CHANGED", user);
    if (
      textContainerElement.children.length - textInputElement.value.length ===
      30
    ) {
      socket.emit("CHARS_TO_FINISH_COMMENTOR");
    }
  }

  function setTimeToEvents() {
    let currentTime = 0;
    intervalHandler();
    function intervalHandler() {
      if (currentTime != 0) {
        socket.emit("REQUEST_FOR_POSITION");
      }
      if (+secondsForGameElement.innerText === 0) {
        clearInterval(interval);
      }
      currentTime++;
    }
    const interval = setInterval(intervalHandler, 30000);
  }
}

socket.on("SHOW_TEXT", showTextHandler);

function showPositionsHandler(progressData) {
  let progressMap = new Map(JSON.parse(progressData));
  let progressArray = Array.from(progressMap, ([name, value]) => ({
    name,
    value,
  }));
  let message = "Positions for now ";
  let leaders = [];
  let leadersPercent = [];
  let messagePart2 = "";

  for (let i = 0; i < progressArray.length; i++) {
    leadersPercent.push(progressArray[i].value);
  }

  leadersPercent.sort(function (a, b) {
    return b - a;
  });

  for (let i = 0; i < leadersPercent.length; i++) {
    for (let j = 0; j < leadersPercent.length; j++) {
      if (leadersPercent[i] == progressArray[j].value) {
        leaders.push(progressArray[j].name);
      }
    }
  }

  let topPositionsNumber = Math.min(3, leaders.length);
  for (let i = 0; i < topPositionsNumber; i++) {
    messagePart2 =
      messagePart2 + "on " + (i + 1) + " place is " + leaders[i] + " ,";
  }
  message = message + messagePart2;
  commentor.changeMessage(message);
}

socket.on("SHOW_POSITIONS", showPositionsHandler);

function updateStatusHandler(user) {
  const userElement = document.querySelector(`.user-${user.id}`);
  if (user.status) {
    userElement.querySelector(".status").classList.add("ready-status-green");
    userElement.querySelector(".status").classList.remove("ready-status-red");
  } else {
    userElement.querySelector(".status").classList.add("ready-status-red");
    userElement.querySelector(".status").classList.remove("ready-status-green");
  }
}

socket.on("UPDATE_STATUS", updateStatusHandler);

function updateProgressBarsHandler(lettersTyped) {
  let parsed = JSON.parse(lettersTyped);
  parsed.forEach((user) => {
    updatePrgressBar(user);
  });
}

socket.on("UPDATE_PROGRESSBARS", updateProgressBarsHandler);

function gameEndHandler(winners) {
  let message = "Winners: ";
  winners.forEach((winner, index) => {
    message += `${index + 1}.${winner.username}, `;
  });
  textInputElement.classList.add("display-none");

  commentor.changeMessage(message);
  showModal(winners);
}

socket.on("GAME_END", gameEndHandler);

socket.on("GREET_USERS", (users) => {
  commentor.greetUsers(users);
});

socket.on("SHOW_COMMENTOR", () => {
  commentor.show();
  commentor.changeMessage("Greetings!");
});

socket.on("ANNOUNCE_WINNER_COMMENTOR", (user) => {
  commentor.announceWinner(user);
});

socket.on("CHARS_TO_FINISH_COMMENTOR", (user) => {
  commentor.charsToFinishAlert(user);
});

// EVENT LISTENERS
addRoomBtn.addEventListener("click", createRoomButtonHandler);
readyBtn.addEventListener("click", readyBtnHandler);
