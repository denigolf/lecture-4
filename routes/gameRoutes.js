import { Router } from 'express';
import path from 'path';
import { HTML_FILES_PATH } from '../config';
import data from '../data';

const router = Router();

router.get('/', (req, res) => {
  const page = path.join(HTML_FILES_PATH, 'game.html');
  res.sendFile(page);
});
router.get('/texts/:id', (req, res) => {
  const textId = req.params.id - 1;
  const text = data.texts[textId];
  if (text) {
    res.send(data.texts[textId]);
  } else {
    res.status(404).send('Error: Text not found.');
  }
});

export default router;
